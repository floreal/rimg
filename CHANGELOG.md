# v0.1.1 - 2020-05-29

## Documentation improvements
 - 1: Enhancement: Basic documentation for already present source code, README and CONTRIBUTING files - https://framagit.org/floreal/rimg/-/issues/1
