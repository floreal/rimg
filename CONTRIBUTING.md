# Contributing to RImg

First of all thank you for contributing to RImg crate. In this file you will learn how to:

  - Setup a proper environment
  - Writing modifications (Bug fixes, New Feature)
  - Submitting your contribution

## Environment setup

First of all you need to install a rust toolchain. There are several way to do so. The best and most common way is using using [rustup](https://rustup.rs) utility.

  1. Install rustup utility accordingly to your platform.
  2. In your favorite shell execute `rustup install stable` to install the stable toolchain

Some other tools may be useful for example for test code coverage you may want to use tarpaulin, or noter in order to build changelog:

```sh
cargo install cargo-tarpaulin
cargo install noter
```

Now your toolchain is installed and ready to be used, fork the main repository through Gitlab interface and then clone it:

```sh
git clone git@framagit.org/<your-username-or-group>/rimg.git
```

## Writing Changes and submitting them

There are three goals to keep up with code quality:

- Accurate tests for new code additions
- Naive implementation to make test pass
- Well documented code (also do not hesitate to write  
  [examples](https://doc.rust-lang.org/rustdoc/documentation-tests.html) in code 
  documentation, cargo will run them for you)

Once you feel ready to propose you changes, create a Merge Request, then your modification will be reviewed and be merged (after pipeline successes).
