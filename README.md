# RImg - Rust Image Template Image Processing Toolkit

[![pipeline status](https://framagit.org/floreal/rimg/badges/master/pipeline.svg)](https://framagit.org/floreal/rimg/-/commits/master)

RImg aims to be a Rust implementation of CImg, the C++ Image Template
Image Processing Toolkit.

Some enhancement from Rust shall also be brought by rust:

- Return [`Result<T, Err>`](https://doc.rust-lang.org/std/result/enum.Result.html) type, 
  while C++ allows throwing exception which Rust do not implement this paradigm
- Return [`Option<T>`](https://doc.rust-lang.org/std/option/enum.Option.html) type instead 
  of C++ NULL pointer

## Usage

For now on this crate is very far from being useful, yet if you want to include it, to your 
dependencies, just edit your Cargo.toml file by adding this:

```toml
[dependencies]

rimg = ">=0.1"
```

then you may use it that way:

```rust
extern crate rimg;
use rimg::RImg;

fn main() {
    let img = RImg::new(128, 256, 1, 3, 0);
    println!("image size: {}", img.size());
}
```

## Documentation

Latest documentation shall be found at [Docs.rs](https://docs.rs/rimg/).

## Contributing

All contribution are welcome. But for now the priority would be to reach CImg
Features.

Read CONTRIBUTING.md file for more information.

## License

RImg is a free, open-source library distributed under the
[LGPL-V3.0](https://www.gnu.org/licenses/quick-guide-gplv3.html) license terms.

